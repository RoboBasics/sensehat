### ***RoboBASics*** ###

![RB Logo.jpg](https://bitbucket.org/repo/9yGMyB/images/2166640969-RB%20Logo.jpg)

This repo contains Python scripts written for the Pi Sense Hat

The SenseHatMain.py script contains:

* joystick driven menu
* live tracking of the ISS station: latitude/longitude, visibility, country, timezone and (some) flags
* a level with 2 crossing visor lines
* examples of all sensors (temperature, pressure, sealevel, IMU)

More info on: http://www.instructables.com/id/Live-ISS-Tracker-Using-the-Raspberry-Pi-With-a-Sen/